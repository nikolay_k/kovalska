/**
 * Author: Nikolay Kovalenko
 * Date: 12.09.2017
 * Email: nikolay.arkadjevi4@gmail.com
 * */

var newYork = {
    header: '.navbar',
    menuWrapper: '.header',
    menuList: '.menu',
    menuitem: 'menuttoggle',
    footer: '.footer',
    banner: '.bottom-fixed-banner',
    contactForm: '#contactForm',
    windows: window,
    chat: '#chat',
    curDevice: window.Detectizr.device.type,
    openSearch: false,


    init: function () {

        try {
            this.initSliders();
        } catch (err) {
            console.log(err);
        }

        setTimeout(function () {
            try {
                $this.headerMenu($this.menuWrapper, $this.menuList);
            } catch (err) {
                console.log(err);
            }
        }, 500);

        this.chatAnimate(this.chat);


        try {
            this.flashcard();
        } catch (err) {
            console.log(err);
        }

        try {
            this.contactValidate(this.contactForm);
        } catch (err) {
            console.log(err);
        }


        try {
            this.bannerScroll(this.banner);
            this.bannerClose(this.banner);
            this.initPopaps();
        } catch (err) {
            console.log(err);
        }

    },

    headerMenu: function (header, menu) {
        $this = this;

        var windows = $(this.windows);

        if (this.curDevice === "tablet") {
            console.log("tru");
            $('.has-dropdown > a').click(function (e) {
                e.preventDefault();
                e.stopPropagation();
            });
        }

        function initDropdown() {
            $(menu).find("li").each(function () {
                if ($(this).children("ul.dropdown").length > 0) {
                    $(this).children("ul.dropdown").addClass("submenu");
                    $(this).children("a").append("<span class='indicator'><i class='fa fa-angle-down'></i></span>");
                }
            })
        }

        function addOverlay() {
            $('body').append("<div class='overlay'></div>");

            $('body').find(".overlay").fadeIn(300).on("click touchstart", function (o) {
                closeMenu();
                $('.menutoggle-close').removeClass('open');
                $('.menutoggle').removeClass('open');
            });

            $('body').find(".menutoggle-close").on("click touchstart", function (o) {
                o.preventDefault();
                closeMenu();
                $(this).removeClass('open');
                $('.menutoggle').removeClass('open');
            })
        }

        function removeOverlay() {
            $('body').find(".overlay").fadeOut(400, function () {
                $(this).remove();
            })
        }

        function openMenu() {
            $(header).on("click touchstart", "a.close-icon-wrap", function (event) {
                event.stopPropagation();
                event.preventDefault();
                closeMenu();
            });
            $(header).find(".menu").addClass("open");
            addOverlay();
        }

        function closeMenu() {
            $(header).find(".menu").removeClass("open");
            removeOverlay();
        }

        function initEvent() {
            $(menu).off("mouseenter mouseleave")
        }

        function openDropdown() {
            initEvent();
            $(menu).on("mouseenter", "li", function () {
                $(this).children("ul.dropdown").css('display', 'none');
                $(this).children("ul.dropdown").stop(!0, !1).fadeToggle(150);
            });

            $(menu).on("mouseleave", "li", function () {
                $(this).children("ul.dropdown").stop(!0, !1).fadeOut(150);
                $(this).removeClass('active');
            })
        }

        function subMenu() {
            initEvent();
            $(menu).find("ul.submenu").hide(0);
            $(menu).find(".indicator").removeClass("indicator-up");
            $(menu).on("click", ".indicator", function (event) {
                return event.stopPropagation(), event.preventDefault(), $(this).parent("a").parent("li").siblings("li").find(".submenu").stop(!0, !0).delay(300).slideUp(300), $(this).closest(".nav-menu").siblings(".nav-menu").children("li").parent("li").siblings("li").find("ul.submenu").stop(!0, !0).delay(300).slideUp(300), "none" == $(this).parent("a").siblings(".submenu").css("display") ? ($(this).addClass("indicator-up"), $(this).parent("a").parent("li").siblings("li").find(".indicator").removeClass("indicator-up"), $(this).closest(".menu").siblings(".menu").find(".indicator").removeClass("indicator-up"), $(this).parent("a").parent("li").children(".submenu").stop(!0, !0).delay(300).slideDown(300), !1) : ($(this).parent("a").parent("li").find(".indicator").removeClass("indicator-up"), void $(this).parent("a").parent("li").find(".submenu").stop(!0, !0).delay(300).slideUp(300))
            });
        }

        function initMenu() {
            window.innerWidth < 768 ? ($(header).addClass("mobile"), subMenu()) : ($(header).removeClass("mobile"), openDropdown(), removeOverlay(), closeMenu())
        }

        $(header).on("click touchstart", ".menutoggle", function (event) {
            event.stopPropagation();
            event.preventDefault();
            openMenu();
            $(this).toggleClass('open');
            $('.menutoggle-close').toggleClass('open');
        });

        initDropdown();
        initMenu();

        windows.resize(function () {
            initMenu();
        });

    },

    checkState: function (btn) {
        var btnClasses = btn.classList;
        if (!btnClasses.contains("open")) {
            btn.classList.add("open");
        } else {
            btn.classList.remove("open");
        }

    },

    chatAnimate: function (chat) {
        // find end of animation css
        function whichAnimationEvent() {
            var t,
                el = document.createElement("fakeelement");

            var animations = {
                "animation": "animationend",
                "OAnimation": "oAnimationEnd",
                "MozAnimation": "animationend",
                "WebkitAnimation": "webkitAnimationEnd"
            };

            for (t in animations) {
                if (el.style[t] !== undefined) {
                    return animations[t];
                }
            }
        }

        var animationEvent = whichAnimationEvent();

        // main
        $(chat).one(animationEvent, function (event) {
        });

        $(chat).find('.chat__close').click(function () {
            $(chat).fadeOut();
        });
    },

    bannerScroll: function (banner) {
        if ($(banner).offset().top + $(banner).height() >= $(this.footer).offset().top - 10) {
            $(banner).css({
                'position': 'absolute',
                'top': '-60px'
            });
        }
        if (window.matchMedia('(max-width: 767px)').matches) {
            $('.bottom-fixed-banner').css({
                'position': 'absolute',
                'top': '-90px'
            });
        }

        if ($(document).scrollTop() + window.innerHeight < $(this.footer).offset().top) {
            $(banner).css({
                'position': 'fixed',
                'top': 'auto'
            });
        }
    },

    bannerClose: function (banner) {
        $('#banner-close').click(function () {
            $(banner).fadeOut();
        });
    },

    flashcard: function () {
        $('.first-col').each(function () {
            if ($(this).html() !== "") {
                $('.first-col').addClass('not-empty');
                $('.newyork-flex-item').addClass('width-33');
            }
        });
    },

    initSlider: function (el, options) {
        $(el).slick(options)
    },

    initSliders: function () {

        $this = this;

        /**
         * Slider Options Data
         */


        var sliderOptions = [
            {
                sliderSelector: '.newyork__popular-essays-slider',
                sliderOptions: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    arrows: false,
                    infinite: false,
                    speed: 1000,
                    autoplay: true,
                    autoplaySpeed: 3000,
					pauseOnHover: true,
                    dots: true,
                    focusOnSelect: false,
                    appendDots: '.slider-navigation-popular'
                }
            },
            {
                sliderSelector: '.newyork__samples-slider',
                sliderOptions: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    infinite: true,
                    speed: 1000,
                    autoplay: true,
                    autoplaySpeed: 3000,
					pauseOnHover: true,
                    dots: true,
                    focusOnSelect: false,
                    appendDots: '.slider-navigation-samples'
                }
            }
        ];


        /*
         * Init SLiders
         */

        enquire.register("(min-width: 768px)", {
            match: function () {
                sliderOptions.forEach(function (el) {
                    $this.initSlider(el.sliderSelector, el.sliderOptions);
                });
            },
            unmatch: function () {
                sliderOptions.forEach(function (el) {
                    $this.initSlider(el.sliderSelector, 'unslick');
                });
            }
        });


    },

    contactValidate: function (form) {
        $(form).validate( {
            rules: {
                InputName: {
                    required: true,
                    minlength: 2
                },
                InputEmail1: {
                    required: true,
                    email: true
                },
                textSubj: {
                    required: true,
                    minlength: 5
                },
                userMessage: {
                    required: true
                }
            },
            messages: {
                InputName: {
                    required: "Please enter a name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                InputEmail1: "Please enter a valid email address",
                textSubj:  {
                    required: "Please enter a subject",
                    minlength: "Your subject must consist of at least 5 characters"
                },
                userMessage: {
                    required: "Please enter a message"
                }

            },
            errorElement: "span",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.parent( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
            }
        } );
    },
    
    initPopaps: function () {

        $('.login').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#InputName',
            modal: false
        });

        $('.popap').magnificPopup({
            type: 'inline',
            preloader: false,
            modal: false
        });


        $(document).on('click', '.register', function(e){

            $.magnificPopup.close();
            $.magnificPopup.open({
                type: 'inline',
                closeOnContentClick: false,
                items: {
                    src: '#registerM'
                }
            });

            e.preventDefault();

        });


        $(document).on('click', '.close-copy', function (e) {
            e.preventDefault();
            $.magnificPopup.close();
        });

    }


};

$(document).ready(
    function () {
        newYork.init();

        $('.newyork__flashcards-isotope').isotope({
            itemSelector: '.col-md-4',
            masonry: {
                columnWidth: 0
            }
        });

        $('.archive-content-isotope').isotope({
            itemSelector: '.col-md-4',
            masonry: {
                columnWidth: 0
            }
        });

    }
);

$(window).scroll(
    function () {
        try {
            newYork.bannerScroll(newYork.banner);
        } catch (err) {
            console.log(err);
        }
    }
);

$(window).resize(function () {

});

