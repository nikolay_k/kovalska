/*
 * Author: Nikolay Kovalenko
 * Date: 08.08.08
 * Site: http://valerykovalska.com
 * Author E-mail: kovalenko_mykola@ukr.net
 */

'use strict';

var ValeryKovalska = {
    header: '.navbar',
    menuWrapper: '.header',
    menuList: 'ul.menu',
    menuList2: 'ul.toggle',
    elinView: false,
    vievVideo: false,
    bagQuantityUpd: false,
    sliderInit: false,

    init: function () {
        var $this = this;

        setTimeout(function () {
            try {
                $this.headerMenu($this.menuWrapper, $this.menuList);
            } catch (err) {
                console.log(err);
            }
        }, 500);


        try {
            this.checkMobileScaleMoal();
        } catch (err) {
            console.log(err);
        }


        this.store.addToCart('.productAddToCart');


        this.initGallery();
        this.sectionFullHeight();
        // /this.pressSlider();
        //this.wowInit();
        this.activeTabs();
        this.showMenu();
        this.switchColumn();
        //this.rightProductHeight();
        this.inputRadio(['input[name="size_radio"]', 'input[name="color_radio"]', 'input[name="payment_radio"]', 'input[name="card-payment"]', 'input[name="billingDetailsPayment"]']);
        this.tooltipWrap();
        //this.productImageFitHeight();
        this.videoInProduct();
        this.productSingleSlider();
        this.bagQuantity();
        this.cartList();
        this.filterToggelMobile();

        this.paymentSelect();

        // this.peymentValidate();
        // this.checkoutValidate();

        this.billingShopping();

        this.headerPopup();
        this.contactsPopap();

        try {
            this.store.storeCheckout();
            this.store.storePayment();
            this.store.storePaymentValidCard();
        } catch (err) {
            console.log(err);
        }

        this.getRegion();


        this.hoverableMenuCart();


        this.mobileProductSlider();

    },

    headerMenu: function (header, menu) {
        var windows = $(this.windows);

        function initDropdown() {
            $(menu).find("li").each(function () {
                if ($(this).children("ul.dropdown").length > 0) {
                    $(this).children("ul.dropdown").addClass("submenu");

                    if (window.innerWidth < 768) {
                        $(this).children("a").append("<span class='indicator'><i class='fa'></i></span>");
                    }

                }
            })
        }

        function addOverlay() {
            $('body').append("<div class='overlay'></div>");
            $('body').find(".overlay").fadeIn(300).on("click touchstart", function (o) {
                closeMenu();
            })
        }

        function removeOverlay() {
            $('body').find(".overlay").fadeOut(400, function () {
                $(this).remove();
            })
        }

        function openMenu() {
            $(header).on("click touchstart", "a.close-icon-wrap", function (event) {
                event.stopPropagation();
                event.preventDefault();
                closeMenu();
            });
            $(header).find("ul.menu").addClass("open");
            addOverlay();
        }

        function closeMenu() {
            $(header).find("ul.menu").removeClass("open");
            removeOverlay();
        }

        function initEvent() {
            $(menu).off("mouseenter mouseleave")
        }

        function openDropdown() {
            initEvent();
            $(menu).on("mouseenter", "li", function () {
                $(this).children("ul.dropdown").css('display', 'none');
                $(this).children("ul.dropdown").stop(!0, !1).fadeToggle(150);
                $(this).addClass('active');
            });

            $(menu).on("mouseleave", "li", function () {
                $(this).children("ul.dropdown").stop(!0, !1).fadeOut(150);
                $(this).removeClass('active');
            })
        }

        function subMenu() {
            initEvent();
            $(menu).find("ul.submenu").hide(0);
            $(menu).find(".indicator").removeClass("indicator-up");
            $(menu).on("click", ".indicator", function (event) {
                return event.stopPropagation(), event.preventDefault(), $(this).parent("a").parent("li").siblings("li").find(".submenu").stop(!0, !0).delay(300).slideUp(300), $(this).closest(".nav-menu").siblings(".nav-menu").children("li").parent("li").siblings("li").find("ul.submenu").stop(!0, !0).delay(300).slideUp(300), "none" == $(this).parent("a").siblings(".submenu").css("display") ? ($(this).addClass("indicator-up"), $(this).parent("a").parent("li").siblings("li").find(".indicator").removeClass("indicator-up"), $(this).closest("ul.menu").siblings("ul.menu").find(".indicator").removeClass("indicator-up"), $(this).parent("a").parent("li").children(".submenu").stop(!0, !0).delay(300).slideDown(300), !1) : ($(this).parent("a").parent("li").find(".indicator").removeClass("indicator-up"), void $(this).parent("a").parent("li").find(".submenu").stop(!0, !0).delay(300).slideUp(300))
            })
        }

        function initMenu() {
            window.innerWidth < 768 ? ($(header).addClass("mobile"), subMenu()) : ($(header).removeClass("mobile"), openDropdown(), removeOverlay(), closeMenu());
        }

        $('header').on("click touchstart", ".menutoggle", function (event) {
            event.stopPropagation();
            event.preventDefault();
            openMenu();
        });

        initDropdown();
        initMenu();

        windows.resize(function () {
            initMenu();
        });

    },

    initGallery: function () {

        var $this = this;
        var curDevice = window.Detectizr.device.type;

        if (curDevice !== "mobile") {
            $('.collection, .gallery').find('.gallery-popap').magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true,
                    preload: [0, 2],
                    tCounter: ''
                },
                mainClass: 'avia-popup mfp-zoom-in',
                removalDelay: 500, //delay removal by X to allow out-animation
                showCloseBtn: true,
                closeMarkup: '<button type="button" class="mfp-close"></button>',
                closeBtnInside: false,
                callbacks: {
                    beforeOpen: function () {
                        // just a hack that adds mfp-anim class to markup
                        this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                        this.st.mainClass = this.st.el.attr('data-effect');
                        //add custom css class for different styling
                        if (this.st.el && this.st.el.data('fixed-content')) {
                            this.fixedContentPos = true;
                        }
                    },

                    change: function () {
                        // console.log('Content changed');
                    },

                    open: function () {

                        $this.vievVideo = true;
                        $('body').addClass('fullscreen fullscreen-mobile');
                        //overwrite default prev + next function. Add timeout for  crossfade animation
                        $.magnificPopup.instance.next = function () {
                            var self = this;
                            self.wrap.removeClass('mfp-image-loaded');
                            setTimeout(function () {
                                $.magnificPopup.proto.next.call(self);
                            }, 120);
                        };
                        $.magnificPopup.instance.prev = function () {
                            var self = this;
                            self.wrap.removeClass('mfp-image-loaded');
                            setTimeout(function () {
                                $.magnificPopup.proto.prev.call(self);
                            }, 120);
                        };

                        //add custom css class for different styling
                        if (this.st.el && this.st.el.data('av-extra-class')) {
                            this.wrap.addClass(this.currItem.el.data('av-extra-class'));
                        }


                    },
                    imageLoadComplete: function () {
                        var self = this;
                        setTimeout(function () {
                            self.wrap.addClass('mfp-image-loaded');
                        }, 16);

                    },
                    beforeClose: function () {
                        var self = this;
                        self.wrap.removeClass('mfp-image-loaded');
                    },
                    close: function () {
                        $('body').removeClass('fullscreen');
                        $('body').removeClass('fullscreen-mobile');
                        // $('.mfp-content figure').trigger('zoom.destroy');

                        $this.vievVideo = false;
                    }
                },
                closeOnContentClick: true,
                midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
            });
        }

        if (curDevice === "mobile") {
            $('a', $('.collection, .gallery')).off('click');
            $('a', $('.collection, .gallery')).removeData('magnificPopup');

            $('.pinch-zoom').each(function () {
                $(this).click(function (e) {
                    e.preventDefault();
                });
                new RTP.PinchZoom($(this), {
                    tapZoomFactor: 1,
                    zoomOutFactor: 1,
                    animationDuration: 300,
                    maxZoom: 2,
                    minZoom: 1,
                    lockDragAxis: true
                });
            });


        }

    },


    cookies: {
        setCookie: function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        },

        getCookie: function (cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        },

        checkCookie: function () {
            var user = getCookie("username");
            if (user != "") {
                alert("Welcome again " + user);
            } else {
                user = prompt("Please enter your name:", "");
                if (user != "" && user != null) {
                    setCookie("username", user, 365);
                }
            }
        }
    },

    sectionFullHeight: function () {
        enquire.register("(min-width: 992px) and (min-height: 650px)", {
            match: function () {
                if ($('.section-fullScreen').length) {
                    var vH = $(window).height();

                    $('.section-fullScreen').each(function () {
                        $(this).css({
                            'height': vH
                        });

                    });

                    $('.production-images').css('height', 'calc(' + vH + 'px - 25%)');
                }


            },
            unmatch: function () {
                $('.section-fullScreen').each(function () {
                    $(this).css(
                        'height', ''
                    );

                });
            }

        });


        enquire.register("(min-width: 992px) and (min-height: 800px)", {
            match: function () {
                if ($('.press-tpl2').length) {

                    var vH = $(window).height();

                    $('.press-tpl2').each(function () {

                        var imagesWrap = $(this).find('.production-tpl3-images');
                        var textWrap = $(this).find('.production-tpl3-text');

                        imagesWrap.css({
                            'height': vH - textWrap.outerHeight()
                        });

                    });

                }

            },
            unmatch: function () {
                $('.press-tpl2').each(function () {

                    var imagesWrap = $(this).find('.production-tpl3-images');

                    imagesWrap.css({
                        'height': ''
                    });

                });
            }

        });
    },

    pressSlider: function () {
        $('.slider-flex .row').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        dots: false,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        dots: true,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        dots: true,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }

            ]

        });

        $(".slider-right").click(function (e) {
            e.preventDefault();
            $(this).closest('.press-tpl3').find('.slider-flex').slick('slickNext');
        });
    },

    activeTabs: function () {
        // Javascript to enable link to tab
        var url = document.location.toString();
        if (url.match('#')) {
            $('.site-header-menu a[href="#' + url.split('#')[1] + '"]').tab('show');
            $('.sidebar-menu a[href="#' + url.split('#')[1] + '"]').tab('show');
            $('html, body').animate({scrollTop: 0});
        }

        // Change hash for page-reload
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // window.location.href = e.target.hash;
        });

        $('.has-sub a').click(function () {
            url = $(this).attr('href');
            $('.site-header-menu a[href="#' + url.split('#')[1] + '"]').tab('show');
        });

        $('.footer_nav-bottom li:nth-child(2) a').click(function () {
            url = $(this).attr('href');
            $('.sidebar-menu a[href="#' + url.split('#')[1] + '"]').tab('show');
        });
        $('.nav_header-right li:nth-child(1)  a').click(function () {
            url = $(this).attr('href');
        });

    },

    showMenu: function () {
        var btn = $('.menu-toggle-inner');
        var menu = '.toggle';

        btn.click(function (e) {

            var $this = this;
            e.preventDefault();
            e.stopPropagation();


            $(this).closest('.header-page, .header-page-contacts').find(menu).toggleClass('active');

            $(this).toggleClass('active');

            $(window).on('click', function (e) {
                if ((e.target !== $this) || document.querySelector()) {
                    if ($(menu).hasClass('active')) {
                        btn.removeClass('active');
                        $(menu).removeClass('active');
                    }
                }
            });

        });
    },

    showMenuOnScroll: function () {
        var menu = $('.hoverable-menu');
        var menuTimeout = null,
            $this = this;

        var curDevice = window.Detectizr.device.type;

        function showMenu() {
            menu.removeClass('hoverable-menu-hidden');
        }

        function hideMenu() {
            menu.addClass('hoverable-menu-hidden');
            $('.toggle').removeClass('active');
            $('header .menu-toggle-inner').removeClass('active');
            $('.hoverable-menu .pop-wrap').removeClass('shoved');
            $('.hoverable-menu .cloned-cart-list').removeClass('active');
        }

        function mouseMoveHandler(e) {

            if ((e.clientY > 0 && e.clientY < 50 && e.pageY > 131) || menu.is(':hover')) {
                clearTimeout(menuTimeout);
                menuTimeout = null;

                if ($this.vievVideo === false) {
                    showMenu();
                }


            } else if (menuTimeout === null) {
                menuTimeout = setTimeout(hideMenu, 700);
            }
        }

        if (window.pageYOffset > 131 && curDevice === "desktop") {
            $(window).on('mousemove', mouseMoveHandler);
            $(window).on('ready', mouseMoveHandler);
        }

    },

    switchColumn: function () {
        // switch to 3-4 column mode
        var $this = this;
        var productsGridEls = $('.products-grid > li');
        var productGrid = $('.category-products .products-grid');
        $('.column-switch a').click(function (e) {
            if (!$(this).hasClass('active')) {
                var clickedModeEl = $(this);
                productGrid.animate({'opacity': '0'}, 500, function () {
                    productsGridEls.css('width', '').removeClass('col-md-4').removeClass('col-md-12').removeClass('col-md-3');

                    if (clickedModeEl.hasClass('three')) {
                        productsGridEls.addClass('col-md-4');
                        //$this.bigProdImgFitWidth();
                    } else if (clickedModeEl.hasClass('four')) {
                        productsGridEls.addClass('col-md-3').addClass('col-md-3');
                        //$('.product-list__item-image').css('height', '');
                        $this.productImageFitHeight();
                    }

                    clickedModeEl.addClass('active').siblings().removeClass('active');
                    productGrid.animate({'opacity': '1'}, 500);
                });
            }
            e.preventDefault();
        });
    },

    rightProductHeight: function () {
        var firstProfImgHeight = $('.product-item__gallery-container .image-container:first-child').height();

        var prodDetailWrap = $('.product-item__detail');
        var prodRelatedlWrap = $('.product-item__related');

        enquire.register("(min-width: 992px)", {
            match: function () {
                prodDetailWrap.css('height', firstProfImgHeight);
                prodRelatedlWrap.css('height', firstProfImgHeight);

                $('.product-item__related .product-list__item-image').css('height', firstProfImgHeight / 2);
            },
            unmatch: function () {
                prodDetailWrap.css('height', '');
                prodRelatedlWrap.css('height', '');

                $('.product-item__related .product-list__item-image').css('height', "");
            }

        });


    },

    inputRadio: function (type) {

        function radio_check(curType) {
            $(curType).each(function () {
                if ($(this).prop("checked")) {
                    $(this).parents('label').addClass('checked currentswatch');
                    $(this).attr('checked', 'checked');
                } else {
                    $(this).parents('label').removeClass('checked currentswatch');
                    $(this).removeAttr('checked');
                }
            });
        }


        for (var i = 0; i < type.length; i++) {

            var el = $(type[i]);

            if (el.length > 0) {
                radio_check(el);
            }

            el.change(function (e) {
                radio_check('input[name="' + e.currentTarget.name + '"]');
            });
        }

    },


    tooltipWrap: function () {
        $('.tooltip-wrap-show').click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            if ($(this).closest('.tooltip-wrap').find('.tooltip-content').hasClass('show')) {
                $(this).closest('.tooltip-wrap').find('.tooltip-content').removeClass('show');
            } else {
                $('.tooltip-content').removeClass('show');
                $(this).closest('.tooltip-wrap').find('.tooltip-content').addClass('show');
            }

        });

        $(document).click(function (e) {
            //console.log(e.target.className);

            if (e.target.className !== "tooltip-content" && e.target.className !== "tooltip-wrap-show") {
                $('.tooltip-content').removeClass('show');
            }
        });

    },

    productImageFitHeight: function () {
        var curDevice = window.Detectizr.device.type;
        var height = $(window).height();
        var imgWrap = $('.category-products .product-list__item-image');

        if (curDevice !== "mobile") {
            enquire.register("(min-width: 768px)", {
                match: function () {
                    imgWrap.css(
                        'height', height / 2
                    );
                },
                unmatch: function () {
                    imgWrap.each(function () {
                        $(this).css(
                            'height', ''
                        );
                    });
                }

            });
        }
    },

    bigProdImgFitWidth: function () {
        var curDevice = window.Detectizr.device.type;
        var height = $(window).width();
        var imgWrap = $('.category-products .product-list__item-image');

        if (curDevice !== "mobile") {
            enquire.register("(min-width: 768px)", {
                match: function () {
                    imgWrap.css(
                        'height', height / 3
                    );
                },
                unmatch: function () {
                    imgWrap.each(function () {
                        $(this).css(
                            'height', ''
                        );
                    });
                }

            });
        }
    },

    videoInProduct: function () {
        var $this = this;

        if ($('.opened-video').length) {


            var video = $(".opened-video").find('iframe');
            var player = new Vimeo.Player(video);

            var offset = $('.opened-video').offset().top;

            $('.startVideo').on('click', function () {
                $('.opened-video').toggleClass('active');
                $('body').toggleClass('fullscreen');

                setTimeout(function () {
                    $('body').animate({
                        scrollTop: offset
                    }, 900);
                    player.play();
                });


                $this.vievVideo = true;

            });

            $(document).on('click', '.opened-video .close', function () {

                $('body').animate({
                    scrollTop: 0
                }, 900);

                $(this).parent().removeClass('active');
                $('body').removeClass('fullscreen');

                player.pause();

                $this.vievVideo = false;
            });
        }
    },

    productSingleSlider: function () {
        enquire.register("(max-width: 991px)", {
            match: function () {
                $('.product-related .showmodels').slick({
                    dots: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                });
            },
            unmatch: function () {
                $('.product-related .showmodels').slick('unslick')
            }

        });
    },

    bagQuantity: function () {
        var $this = this;
        jQuery('.quantity').each(function () {
            var spinner = jQuery(this),
                input = spinner.find('input#input-quantity'),
                btnUp = spinner.find('.quantity-up'),
                btnDown = spinner.find('.quantity-down'),
                min = input.attr('min'),
                max = input.attr('max');

            btnUp.click(function () {
                var oldValue = parseFloat(input.val());

                var queryUrl = $(this).attr('data-up-url');
                var queryProductID = $(this).attr('data-product-id');

                if (oldValue >= max) {
                    var newVal = oldValue;
                } else {
                    if (oldValue === "1") {
                        newVal = oldValue;
                    } else {
                        newVal = oldValue + 1;
                    }
                }
                spinner.find("input#input-quantity").val(newVal);
                spinner.find("input#input-quantity").trigger("change");

                $this.store.updateProductCountBag(queryProductID, queryUrl, newVal);
            });

            btnDown.click(function () {
                var oldValue = parseFloat(input.val());
                var newVal = 0;
                var queryUrl = $(this).attr('data-down-url');
                var queryProductID = $(this).attr('data-product-id');

                if (oldValue <= min) {
                    newVal = oldValue;
                } else {
                    if (oldValue === "1") {
                        newVal = oldValue;
                    } else {
                        newVal = oldValue - 1;
                    }
                }
                spinner.find("input#input-quantity").val(newVal);
                spinner.find("input#input-quantity").trigger("change");

                $this.store.updateProductCountBag(queryProductID, queryUrl, newVal);
            });

        });
    },

    cartList: function () {
        $('.header-home .cartItemList').on('click', function (event) {
            event.preventDefault();
            $(this).closest('.header-home').find('#cartWidget .cart-list').toggleClass('active');
        });

        $('.header-page-inner .cartItemList').on('click', function (event) {
            event.preventDefault();
            $(this).closest('.header-page-inner').find('#cartWidget .cart-list').toggleClass('active');
        });


        $(window).on('click', function (event) {
            var cartList = document.getElementsByClassName('cart-list')[0];
            var cartButton = document.getElementsByClassName('cartItemList')[0];
            var target = event.target;
            if ((target == cartList) || (target.parentElement == cartList) || (target == cartButton)) {
            } else {
                if ($('.cart-list').hasClass('active')) {
                    $('.cart-list').removeClass('active')
                }
            }
        });
    },

    filterToggelMobile: function () {
        $('.filter-button').on('click', function () {
            $('.cats').toggleClass('active');
        });
    },

    paymentSelect: function () {
        $('select').each(function () {
            var $this = $(this), numberOfOptions = $(this).children('option').length;

            $this.addClass('select-hidden');
            $this.wrap('<div class="select"></div>');
            $this.after('<div class="select-styled"></div>');

            var $styledSelect = $this.next('div.select-styled');
            $styledSelect.text($this.children('option:selected').text());

            var $list = $('<ul />', {
                'class': 'select-options'
            }).insertAfter($styledSelect);

            for (var i = 0; i < numberOfOptions; i++) {
                $('<li />', {
                    text: $this.children('option').eq(i).text(),
                    rel: $this.children('option').eq(i).val()
                }).appendTo($list);
            }

            var $listItems = $list.children('li');

            $styledSelect.click(function (e) {
                e.stopPropagation();
                $('div.select-styled.active').not(this).each(function () {
                    $(this).removeClass('active').next('ul.select-options').hide();
                });
                $(this).toggleClass('active').next('ul.select-options').toggle();
            });

            $listItems.click(function (e) {
                e.stopPropagation();
                $styledSelect.text($(this).text()).removeClass('active');
                $this.val($(this).attr('rel'));
                $list.hide();
                //console.log($this.val());
            });

            $(document).click(function () {
                $styledSelect.removeClass('active');
                $list.hide();
            });

        });
    },

    peymentValidate: function () {
        if ($('#paymentForm').length) {
            $('#paymentForm').validate({
                rules: {
                    cNamePayment: {
                        required: true,
                        minlength: 2
                    },
                    cvvPayment: {
                        required: true,
                        minlength: 3,
                        maxlength: 3
                    }
                },
                messages: {
                    cNamePayment: {
                        required: "Please enter a name",
                        minlength: "Cardholder's name must consist of at least 2 characters"
                    },
                    cvvPayment: {
                        required: "Please enter a subject",
                        minlength: "Your CVV must consist of at least 3 characters",
                        maxlength: ""
                    }
                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    error.addClass("help-block");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        // error.insertAfter( element );
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).parents(".form-group").addClass("has-error").removeClass("has-success");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".form-group").addClass("has-success").removeClass("has-error");
                }
            });
        }
    },

    checkoutValidate: function () {
        if ($('#shippingForm').length) {
            $('#shippingForm').validate({
                rules: {
                    firstNameShipping: {
                        required: true,
                        minlength: 2
                    },
                    lastNameShipping: {
                        required: true,
                        minlength: 2
                    },
                    addressShipping: {
                        required: true,
                        minlength: 2
                    },
                    countryShipping: {
                        required: true,
                        minlength: 2
                    },
                    cityShipping: {
                        required: true,
                        minlength: 2
                    },
                    zipShipping: {
                        required: true,
                        minlength: 1
                    },
                    phoneShipping: {
                        required: true,
                        minlength: 7
                    }
                },
                messages: {
                    firstNameShipping: {
                        required: "Please enter a name",
                        minlength: "Your name must consist of at least 2 characters"
                    },
                    lastNameShipping: {
                        required: "Please enter a name",
                        minlength: "Your name must consist of at least 2 characters"
                    }
                },
                errorElement: "em",
                errorPlacement: function (error, element) {
                    error.addClass("help-block");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        // error.insertAfter( element );
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).parents(".form-group").addClass("has-error").removeClass("has-success");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".form-group").addClass("has-success").removeClass("has-error");
                }
            });
        }

    },


    billingShopping: function () {
        function ckeck() {
            var checkbox = $('input[name="billingDetailsPayment"]').is(':checked');

            checkbox ? $(".billing-details-data").hide() : $(".billing-details-data").show();
        }

        ckeck();
        $('input[name="billingDetailsPayment"]').change(ckeck)
    },

    loadProduct: function () {
        var $this = this;
        var urlSelector = $('.product-list__loading-wrapper[data-load="unload"]'),
            url = urlSelector.attr("data-next-producturl"),
            curCat = urlSelector.attr('data-current-category');


        var processing = false;

        if (urlSelector.length && $this.isScrolledIntoView(urlSelector)) {
            processing = true;

            urlSelector.attr("data-load", "loaded");

            var data = {
                "data-next-product": urlSelector.attr('data-next-product')
            };

            if (curCat && curCat !== undefined && curCat !== null) {
                data["data-current-category"] = curCat;
            }


            $.ajax({
                type: "GET",
                dataType: "html",
                url: url,
                data: data,
                success: function (response) {
                    urlSelector.removeClass("product-list__loading-wrapper");
                    urlSelector.attr("data-load", "loaded");
                    urlSelector.remove();
                    $('body').find('.product-item__eos')[0].remove();

                    $('main.page').append(response);

                    //$this.rightProductHeight();

                    $this.inputRadio(['input[name="size_radio"]', 'input[name="color_radio"]']);

                    $this.tooltipWrap();

                    processing = false;


                    try {
                        $this.context.getContextEls();
                        $this.context.bindEvents();
                    } catch (err) {
                        console.log(err);
                    }

                    $this.updateURLwhileScrolling(".single.store.context--active", "product-url");


                    $this.initZoom();

                    return false;
                }
            });
        }

    },

    loadInstagramFirst: function () {
        var $this = this;
        var urlSelector = $('.instagram'),
            firstUrl,
            instagram = urlSelector.find('.instagram-wrapper'),
            instagramToAppend = urlSelector.find('.instagram-wrapper[data-toLoad="instagram"]');

        if (urlSelector.length) {
            firstUrl = urlSelector.attr('data-url-first-load');
        }

        var processing = false;

        if (instagramToAppend.length) {
            processing = true;

            $.ajax({
                type: "GET",
                dataType: "html",
                url: firstUrl,
                success: function (response) {

                    if (response && response !== "" && response !== undefined) {

                        instagram.append(response);

                        //console.log(response);

                        instagram.attr('data-instagram-load', 'true');
                        processing = false;
                        return false;
                    }

                }
            });
        }

    },


    loadInstagramMore: function () {
        var $this = this;
        var urlSelector = $('.insta__loading-wrapper'),
            wrapper = $('.instagram'),
            urlSelectorOnScroll = $('.insta__loading-wrapper[data-load="unload"]'),
            instagram = wrapper.find('.instagram-wrapper'),
            loadMoreUrl;

        if (urlSelector.length) {
            loadMoreUrl = urlSelector.attr('data-load-instagram-url');
        }

        var processing = false;

        if (urlSelector.length && $this.isScrolledIntoView(urlSelectorOnScroll)) {
            processing = true;

            urlSelector.attr("data-load", "loaded");

            var data = {
                "data-next-instagram-page": urlSelector.attr('data-next-instagram-page')
            };


            $.ajax({
                type: "GET",
                dataType: "html",
                url: loadMoreUrl,
                data: data,
                success: function (response) {
                    urlSelector.removeClass("insta__loading-wrapper");
                    urlSelector.attr("data-load", "loaded");
                    urlSelector.remove();

                    $('body').find('.product-item__eos')[0].remove();

                    instagram.append(response);

                    processing = false;
                    return false;
                }
            });
        }

    },


    isScrolledIntoView: function (elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = elem.offset().top;
        var elemBottom = elemTop + elem.height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    },


    collectonOnScroll: function (windowEL, lastScrollTop) {
        var collectionList = $('.collection[data-parallax="true"]').find('.gallery');

        collectionList.each(function (index, el) {
            // var el = collectionList[2];
            var img = $(el).find('.item-gallery');
            var pageHeight = $('body').height();
            var windowSroll = $(windowEL).scrollTop();
            var elHeight = $(el)[0].clientHeight;
            var elOffset = $(el).offset().top;
            var pageKof = (($(window).height() / 6) * 5);
            var pageKofBack = (($(window).height() / 6));
            // var pos = windowSroll - (elOffset - pageKof);
            // var pagePercent = (pos * 100) / pageHeight;
            var bg = new Image();
            bg.src = $(img).css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");
            var bgH = bg.naturalHeight;
            var bgW = bg.naturalWidth;
            var bgProp = ($(el).width()) / (bgW);
            var bgH1 = bgH * (bgProp);

            var st = windowSroll;
            if (st > lastScrollTop) {
                var pos = windowSroll - (elOffset - pageKof);
                var pagePercent = (pos * 100) / pageHeight;
                var newPercant = (elHeight * pagePercent) / 100;
                if ((windowSroll > (elOffset - pageKof)) && (windowSroll < elOffset + elHeight)) {
                    if (bgH1 > (elHeight + newPercant)) {
                        $(img).css('background-position-y', newPercant * (-1) + 'px');
                    }
                }
            } else {

                var pos = elHeight + pageKofBack;
                var ws = ((windowSroll - elOffset) * 100) / pos;
                var newPercant = (( pageKofBack + elHeight ) * ws) / 100;

                if ((windowSroll < (pos + elOffset)) && (windowSroll > elOffset )) {
                    if ((bgH1 - elHeight) > ws) {
                        $(img).css('background-position-y', (ws * (-1)) + 'px');
                    }
                }
            }
            lastScrollTop = st;

        });
    },

    headerPopup: function () {

        $('.nav_header-right .pop a').on('click', function (e) {
            var $this = this;
            e.preventDefault();
            e.stopPropagation();
            var popap = $(this).closest('.pop').find('.pop-wrap');
            popap.toggleClass('shoved');


            $(window).on('click', function (event) {
                var popapLink = $this;

                var popap = $($this.parentElement).find('.pop-wrap')[0];
                var target = event.target;
                if ((target !== popapLink) || (target !== popap)) {
                    if ($(popap).hasClass('shoved')) {
                        $(popap).removeClass('shoved');
                    }
                }
            });
        });
    },

    /* Single Store Load More products And Change Url */


    /* Update Url */
    updateURLwhileScrolling: function (el, elDataPar) {
        var $this = this;
        $(window).on("scroll", function () {
            var newUrl = $(el).data(elDataPar);
            if (void 0 !== newUrl && null !== newUrl && newUrl !== window.location.pathname) {
                window.history.replaceState("{}", "", newUrl);
                $this.mobileProductSliderOnScroll(el);
            }
        });
    },

    /* Update Context When Page Scroll */
    context: {
        context: null,
        offset: 400,
        contextEl: null,
        contextEls: [],
        bindEvents: function () {
            $(window).on("scroll", this.findContext.bind(this));
        },
        getContextEls: function () {
            return this.contextEls = $("[data-context]"), this.contextEls;
        },

        unsetCurrentContext: function () {
            if (this.context) {
                this.contextEl.classList.remove("context--active");
                this.contextEl = null;
                this.context = null;
            }
        },

        setContext: function (el) {
            if (el) {
                this.contextEl = el;
                this.context = el.getAttribute("data-context");
                this.contextEl.classList.add("context--active");
            }
        },

        findContext: function () {
            for (var a = 0, b = !1, c = this.contextEls, d = this; a < c.length && !b;) !function (c) {
                d.isActiveContext(c) && (c !== d.contextEl && (d.unsetCurrentContext(), d.setContext(c)), b = !0), a++
            }(c[a]);
            b || d.unsetCurrentContext()
        },

        isActiveContext: function (el) {
            var b, c = el.getBoundingClientRect();
            return b = $(".site-header.toolbar, .header.header-page").length > 0 ? $(".header").height() : 2 * $(".header").height(), c.bottom >= this.offset + b && c.top <= this.offset
        }
    },


    checkMobileScaleMoal: function () {
        var curDevice = window.Detectizr.device.type,
            scaleHelper = $('.scaleImageOnMobile'),
            hasScaleZoom = false;

        if ((scaleHelper.hasClass('showen')) && (curDevice === "mobile")) {
            scaleHelper.fadeIn(500);
            scaleHelper.addClass('open');
        }

        $(document).on('click', '.scaleImageOnMobile-close', function () {
            scaleHelper.fadeOut(1000);
            scaleHelper.removeClass('open');
        })

    },


    /* Store Functions */

    store: {

        productForm: "",
        cartWidget: $('#cartWidget'),
        cartWidgetCloned: $('#cartWidgetCloned'),
        addTocartUrl: "",
        cartWidgetCount: $('.cartItemList-total'),

        /* Product Add To Cart */
        addToCart: function (el) {
            var $this = this,
                productAddedModal = $('.productAdded');

            $(document).on('click', el, function (e) {
                e.preventDefault();

                var productForm = $(this).closest('.productAddForm');

                var addToCartUrl = productForm.attr('data-ajax-url');

                var data = {
                    "color_radio": productForm.find('input[name="color_radio"]:checked').val(),
                    "size_radio": productForm.find('input[name="size_radio"]:checked').val(),
                    "product": productForm.find('input[name="product"]').val(),
                    "price": productForm.find('input[name="price"]').val()
                };

                $.ajax({
                    type: "GET",
                    dataType: "html",
                    url: addToCartUrl,
                    data: data,
                    success: function (response) {
                        //console.log("Product Added");
                        if (response !== undefined || response !== null) {
                            $this.updateCatWidget(response);
                        }

                        var newCartQty = $('#cart-qty').text().trim().toString();

                        $this.updateCount(newCartQty, $this.cartWidgetCount);

                        productAddedModal.addClass('open').fadeIn(2500);
                        //$('body').addClass('fullscreen fullscreen-mobile');

                        setTimeout(function () {
                            productAddedModal.fadeOut(2500).removeClass('open');
                            //$('body').removeClass('fullscreen fullscreen-mobile');
                        }, 2000);

                        /*$('html,body').animate({
                         scrollTop: 0
                         }, 700);*/
                    }

                });

            });


        },

        /* Update Store CartWidget */
        updateCatWidget: function (html) {
            this.cartWidget.find('.cart-list').remove();
            this.cartWidgetCloned.find('.cloned-cart-list').remove();
            this.cartWidget.append(html);

            ValeryKovalska.cloneMenu();
        },

        updateCount: function (count, el) {
            el.text('');
            el.text(count);
        },


        /* Bag Actions */

        deleteProductFromBag: function (product, url) {

            var data = {
                product: product
            };

            $.ajax({
                type: "GET",
                dataType: "JSON",
                url: url,
                data: data,
                success: function (response) {
                    if (response !== null && response.response !== undefined && response.response !== false) {
                        window.location.href = response.response;
                    }
                }

            });

        },

        updateProductCountBag: function (product, url, val) {

            var data = {
                product: product,
                quantity: val
            };

            $.ajax({
                type: "GET",
                dataType: "JSON",
                url: url,
                data: data,
                success: function (response) {
                    if (response !== null && response.response !== undefined && response.response !== false) {
                        window.location.href = response.response;
                    }
                }

            });

        },


        /* Checkout Actions */

        storeCheckout: function () {

            $('#shippingForm').on('submit', function (e) {
                e.preventDefault();

                var url = $(this).attr('action'),
                    success = true,
                    checkoutAlert = $('.checkoutAlert');

                var data = $(this).serialize();

                checkoutAlert.hide();
                checkoutAlert.text('');
                checkoutAlert.closest('.form-gr').removeClass('has-error');
                checkoutAlert.closest('.form-group').removeClass('has-error');

                $.ajax({
                    type: 'GET',
                    url: url,
                    dataType: "JSON",
                    data: data,
                    success: function (response) {
                        if (response.success === false) {

                            $.each(response.errors, function (k, v) {
                                $("." + k + "-alert").addClass('error');
                                $("." + k + "-alert").text(v);
                                $("." + k + "-alert").show();
                                $("." + k + "-alert").closest('.form-group').addClass('has-error');
                            });

                            success = false;

                        } else if (response.success === true) {
                            checkoutAlert.removeClass('error');
                            checkoutAlert.hide();
                            checkoutAlert.text('');

                            window.location.href = response.response;

                            success = true;
                        }
                    }
                });

                return success;
            });
        },

        storePayment: function () {

            $(document).on('click', '.btn-proceed', function () {
                $('.cardNumShipping-alert').closest('.form-gr').addClass('has-error');
            });

            $('#paymentForm').on('submit', function (e) {
                e.preventDefault();

                var url = $(this).attr('action'),
                    success = true,
                    checkoutAlert = $('.checkoutAlert'),
                    $selectAlert = $('.select-alert');

                var data = $(this).serialize();

                checkoutAlert.hide();
                checkoutAlert.text('');

                $selectAlert.hide();
                $selectAlert.text('');

                checkoutAlert.closest('.form-gr').removeClass('has-error');
                checkoutAlert.closest('.form-group').removeClass('has-error');

                $('.cardNumShipping-alert').closest('.form-gr').removeClass('has-error');

                if ($('select#mounth').val() === "" || $('select#year').val() === "") {
                    $('select#mounth').closest('.form-group').addClass('has-error');
                    $selectAlert.text('Please, choose date');
                    $selectAlert.show();
                    success = false;
                }


                $.ajax({
                    type: 'GET',
                    url: url,
                    dataType: "JSON",
                    data: data,
                    success: function (response) {
                        if (response.success === false) {

                            $('.cardNumShipping-alert').closest('.form-gr').addClass('has-error');

                            $.each(response.errors, function (k, v) {
                                $("." + k + "-alert").addClass('error');
                                $("." + k + "-alert").text(v);
                                $("." + k + "-alert").show();
                            });

                            success = false;

                        } else if (response.success === true) {
                            $('.cardNumShipping-alert').closest('.form-gr').removeClass('has-error');
                            checkoutAlert.removeClass('error');
                            checkoutAlert.hide();
                            checkoutAlert.text('');


                            $selectAlert.hide();
                            $selectAlert.text('');

                            window.location.href = response.response;

                            success = true;
                        }
                    }
                });

                return success;
            });
        },

        storePaymentValidCard: function () {

            var success = true;

            $('#cardNum-payment').on('keyup', function (e) {
                var cardNum = $(this).val();

                var data = {
                    "cardNumShipping": cardNum
                };

                if ($(this).val().length > 1) {
                    $.ajax({
                        type: 'GET',
                        url: $(this).attr('data-url-check-cart'),
                        dataType: "JSON",
                        data: data,
                        success: function (response) {
                            if (response.response !== undefined && response.response !== false) {
                                $("label." + response.response).addClass('checked currentswatch');
                                $("input#card-payment-" + response.response).attr("checked", "checked");
                            }
                            success = true;
                        }
                    });
                }
            });
        },
    },


    loadProductsInCategory: function () {
        var $this = this;
        var urlSelector = $('.category-list__loading-wrapper[data-load="unload"]'),
            url = urlSelector.attr("data-load-more-url"),
            curCat = urlSelector.attr('data-category'),
            $productGridWrapper = $('.products-grid');

        var data = {
            'data-info-data': urlSelector.attr('data-info-data'),
            'data-next-data': urlSelector.attr('data-next-data'),
            'data-next-page': urlSelector.attr('data-next-page')
        };


        if (urlSelector.length && $this.isScrolledIntoView(urlSelector)) {
            urlSelector.attr("data-load", "loaded");

            if (curCat && curCat !== undefined && curCat !== null) {
                data["data-category"] = curCat;
            }


            $.ajax({
                type: "GET",
                dataType: "html",
                url: url,
                data: data,
                success: function (response) {
                    urlSelector.removeClass("category-list__loading-wrapper");
                    urlSelector.attr("data-load", "loaded");
                    urlSelector.remove();
                    $('body').find('.category-item__eos')[0].remove();
                    $('body').find('.category-item.clearfix')[0].remove();

                    $this.appendsProducts('.product-list__item-last[data-load-more="unload"]', response);

                    if ($this.settings.checkActiveSwitch() === 'three') {
                        $productGridWrapper.find('.product-list__item.hidden').removeClass('col-md-3').addClass('col-md-4');
                        $productGridWrapper.find('.product-list__item').removeClass('hidden');
                    } else if ($this.settings.checkActiveSwitch() === 'four') {
                        $productGridWrapper.find('.product-list__item.hidden').removeClass('col-md-4').addClass('col-md-3');
                        $productGridWrapper.find('.product-list__item').removeClass('hidden');
                    }

                    $this.switchColumn();

                    //$this.productImageFitHeight();
                    //$this.bigProdImgFitWidth();

                    $($('.product-list__item.product-list__item-last[data-load-more="unload"]')[0]).attr('data-load-more', 'loaded');


                    return false;
                }
            });
        }

    },


    appendsProducts: function (el, content) {
        $(el).after(content);
    },

    getRegion: function () {
        var showRegion = $('.showRegion'),
            success = false;

        showRegion.text();

        $.ajax({
            type: 'GET',
            url: showRegion.attr('data-url-geo'),
            dataType: "JSON",
            success: function (response) {
                if (response !== undefined && response !== false) {
                    showRegion.text(response.country_name);
                }
                success = true;
            },
            async: false
        });

        if (success === false) {
            showRegion.text('United States');
        }

    },

    initZoom: function () {
        var that = this;
        if (that.settings.checkDevice() === "desktop") {
            $('.product-item__gallery-item-image').trigger('zoom.destroy');
            $('.product-item__gallery-item-image').zoom({
                on: 'click',
                onZoomIn: function () {
                    $(this).parent('.product-item__gallery-item-image').addClass('ZoomIn');
                },
                onZoomOut: function () {
                    $(this).parent('.product-item__gallery-item-image').removeClass('ZoomIn');
                }
            });
        }
    },


    hoverableMenuCart: function () {

        ValeryKovalska.cloneMenu();

        $('.hoverable-menu .cartItemList').on('click', function (event) {
            event.preventDefault();
            $(this).closest('.hoverable-menu').find('.cloned-cart-list').toggleClass('active');
        });


        $(window).on('click', function (event) {
            var cartList = document.querySelector('.hoverable-menu .cloned-cart-list');
            var cartButton = document.querySelector('.hoverable-menu .cartItemList');
            var target = event.target;
            if ((target == cartList) || (target.parentElement == cartList) || (target == cartButton)) {
            } else {
                if ($('.hoverable-menu .cloned-cart-list').hasClass('active')) {
                    $('.hoverable-menu .cloned-cart-list').removeClass('active')
                }
            }
        });

    },

    cloneMenu: function () {
        var newel = $('.cart-list').clone().prop({'class': 'cloned-cart-list'});
        $('.hoverable-menu #cartWidgetCloned').append(newel);
    },


    mobileProductSlider: function () {

        var images = $('.mobileGallery-images');

        if (ValeryKovalska.settings.checkDevice() === "mobile" || ValeryKovalska.settings.checkDevice() === "tablet") {
            $('.mobileImageSlider').append(images);

            $('.mobileImageSlider').slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false
            });

        }

    },

    mobileProductSliderOnScroll: function (el) {
        var images =$(el).find('.mobileGallery-images');

        if (this.sliderInit === false) {
            if (this.settings.checkDevice() === "mobile" || this.settings.checkDevice() === "tablet") {
                $(el).find('.mobileImageSlider').append(images);
                $(el).find('.mobileImageSlider').slick({
                    dots: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                });

                $(el).find('.product-related .showmodels').slick({
                    dots: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                });
            }
        }

    },

    settings: {

        checkDevice: function () {
            return window.Detectizr.device.type;
        },

        checkActiveSwitch: function () {
            var switchDisplay = ['three', 'four'],
                switchCurrentArrIndex = 0;
            $('.column-switch a').each(function (index, el) {
                if ($(this).hasClass('active')) {
                    switchCurrentArrIndex = index;
                }
            });

            return switchDisplay[switchCurrentArrIndex];
        }
    },

    contactsPopap: function () {
        $('.stocklist .pop').on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('.pop-wrap').removeClass('shoved');
            var popap = $(this).find('.pop-wrap');
            popap.toggleClass('shoved');
        });

        $(window).on('click', function() {
            var popap = $('.pop-wrap');
            $(popap).removeClass('shoved');
        });

    }

};

$(document).ready(
    function () {
        ValeryKovalska.init();

        ValeryKovalska.loadInstagramFirst();
        ValeryKovalska.initZoom();

        if (ValeryKovalska.settings.checkDevice() === "desktop" || ValeryKovalska.settings.checkDevice() === "tablet") {
            if ($('.scrollbar-inner').length) {
                $('.scrollbar-inner').scrollbar();
            }
        }

        $('*[title]').tooltip('disable');

        if (ValeryKovalska.settings.checkDevice() === "mobile") {
            ValeryKovalska.pressSlider();
        }


    }
);

$(document).ready(function(){
    $('.scroll').stick_in_parent({
        offset_top: 0
    });
});


$(window).scroll(
    function () {
        ValeryKovalska.showMenuOnScroll();
        ValeryKovalska.loadProduct();
        ValeryKovalska.loadProductsInCategory();
        ValeryKovalska.loadInstagramMore();
    }
);

$(window).resize(function () {
    ValeryKovalska.sectionFullHeight();
    //ValeryKovalska.productImageFitHeight();
});


/*OLD FUNC*/

$(window).load(function () {


    setTimeout(function () {
        scrollTo(0, -1);
    }, 0);


    $("[data-fancybox]").fancybox({
        onActivate: function () {
            ValeryKovalska.vievVideo = true;

        },
        afterClose: function () {
            ValeryKovalska.vievVideo = false;
        }
    });


    scaleVideoContainer();
    initBannerVideoSize('.header-video-container video');

    $(window).on('resize', function () {
        scaleVideoContainer();

        scaleBannerVideoSize('.header-video-container video');
    });

});


function scaleBannerVideoSize(element) {

    var windowWidth = $(window).width(),
        windowHeight = $(window).height(),
        videoWidth,
        videoHeight;


    $(element).each(function () {
        var videoAspectRatio = $(this).data('height') / $(this).data('width'),
            windowAspectRatio = windowHeight / windowWidth;

        if (videoAspectRatio > windowAspectRatio) {
            videoWidth = windowWidth;
            videoHeight = videoWidth * videoAspectRatio;
            $(this).css({'top': -(videoHeight - windowHeight) / 2 + 'px', 'margin-left': 0});
        } else {
            videoHeight = windowHeight;
            videoWidth = videoHeight / videoAspectRatio;
            $(this).css({'margin-top': 0, 'margin-left': -(videoWidth - windowWidth) / 2 + 'px'});
        }

        $(this).width(videoWidth).height(videoHeight);

        $('.header-video-wrapper .header-video-container video').addClass('fadeIn animated');
    });
}

scaleVideoContainer();


function scaleVideoContainer() {
    var height = $(window).height();
    var unitHeight = parseInt(height) + 'px';

    $('.header-video-wrapper').css('height', unitHeight);
}


function initBannerVideoSize(element) {
    $(element).each(function () {
        $(this).data('height', $(this).height());
        $(this).data('width', $(this).width());
    });

    scaleBannerVideoSize(element);
}

function headerSize() {
    function changeHeaderSize() {
        var vW = $(window).width(),
            vH = $(window).height();

        if ($('.header-fullScreen').length) {
            $('.header-fullScreen').css({
                'height': vH
            });
        }
    }

    changeHeaderSize();

    $(window).on('resize', function () {
        changeHeaderSize();
    });
}

var targetAnimate = function () {
    $('.icon_arrow_click').click(function (e) {
        e.preventDefault();
        var target = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(target).offset().top
        }, {
            duration: 1000,
            specialEasing: {
                height: "easeInOutExpo"
            },
            complete: function () {
            }
        });

        return false;
    });
};

(function ($) {
    jQuery.fn.maxHeight = function () {
        var blockElement = this.find('img'),
            blockHeightArray = [];
        $(window).on('resize load', function () {
            $.each(blockElement, function (i, element) {
                blockHeightArray.push($(element).height());
            });

            blockHeightArray.sort(function (a, b) {
                return b - a;
            });

            blockElement.parent().css({
                'max-height': blockHeightArray[blockHeightArray.length - 1]
            });

            blockHeightArray = [];
        });

        return this;
    };
})(jQuery);


headerSize();
targetAnimate();



