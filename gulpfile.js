var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    cache = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer'),
    rigger = require('gulp-rigger'),
    browserSync = require('browser-sync'),
    //copy = require('gulp-contrib-copy'),
    uncss = require('gulp-uncss');
// webserver = require('gulp-webserver');


gulp.task('uncss', function() {
    return gulp.src('app/css/dist/style.css')
        .pipe(uncss({
            html: ['app/old.html']
        }))
        .pipe(gulp.dest('./out'));
});


gulp.task('html', function() {
    gulp.src('app/pages/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('app'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('sass', function() {
    return gulp.src('app/scss/**/*.+(scss|sass)')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(gulp.dest('app/css/dist'))
        .pipe(browserSync.reload({ stream: true }))
});


gulp.task('css-libs', ['sass'], function() {
    return gulp.src('app/css/libs.css')
        .pipe(cssnano())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('app/css'));
});


gulp.task('img', function() {
    return gulp.src('app/img/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});


gulp.task('clean', function() {
    return del.sync('dist'); // Удаляем папку dist перед сборкой
});

gulp.task('clear', function() {
    return cache.clearAll();
});

gulp.task('build', ['clean', 'sass'], function() {
    var buildCss = gulp.src([
        'app/css/main.css',
        'app/css/libs.min.css'
    ])
        .pipe(gulp.dest('dist/css'));
    var buildFonts = gulp.src('app/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'));

    var buildJs = gulp.src('app/js/**/*')
        .pipe(gulp.dest('dist/js'));

    var buildHtml = gulp.src('app/*.html')
        .pipe(gulp.dest('dist'));

});

gulp.task('watch', ['browser-sync', 'html', 'sass'], function() {
    gulp.watch('app/scss/**/*.+(scss|sass)', ['sass']);
    gulp.watch('app/pages/*.html', ['html']);
    gulp.watch('app/pages/core/*.html', ['html']);
    gulp.watch('app/pages/templates/*.html', ['html']);
    gulp.watch('app/pages/templates/**/*.html', ['html']);
    gulp.watch('app/pages/*/*.html', ['html']);
    gulp.watch('app/js/**/*.js', browserSync.reload);
});


gulp.task('default', ['watch']);